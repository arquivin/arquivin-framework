package com.arquivin.framework.utils.jwt.util;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CorsUtils {

	/**
	 * Propaga headers da request.
	 * 
	 * @param request
	 * @param response
	 * @return 
	 */
	@SuppressWarnings("unchecked")
	public static HttpServletResponse ecoHeaders(HttpServletRequest request, HttpServletResponse response) {
		Enumeration<String> enumeration = request.getHeaderNames();
		while (enumeration.hasMoreElements()) {
			String headerName = enumeration.nextElement();
			if(!response.containsHeader(headerName)){
				response.setHeader("Access-Control-Allow-Headers", headerName);
			}
		}
		return response;
	}
	
	public static HttpServletResponse setBasicHeaders(HttpServletResponse response){
		if(!response.containsHeader("Access-Control-Allow-Origin")){
			response.addHeader("Access-Control-Allow-Origin", "*");
		}
		
		if(!response.containsHeader("Access-Control-Expose-Headers")){
			response.addHeader("Access-Control-Expose-Headers", "Authorization");
		}
		return response;
	}
	
	public static HttpServletResponse setOptionsHeaders(HttpServletResponse response){
		if(!response.containsHeader("Access-Control-Allow-Methods")){
			response.addHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
		}
		if(!response.containsHeader("Access-Control-Allow-Headers")){
			response.addHeader("Access-Control-Allow-Headers", "*, Content-Type, Authorization");
		}
		if(!response.containsHeader("Access-Control-Max-Age")){
			response.addHeader("Access-Control-Max-Age", "1");
		}
		return response;
	}

}
