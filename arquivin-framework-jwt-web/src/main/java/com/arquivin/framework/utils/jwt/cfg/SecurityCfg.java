package com.arquivin.framework.utils.jwt.cfg;

import javax.sql.DataSource;

import org.jasypt.spring.security3.PBEPasswordEncoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.arquivin.framework.utils.jwt.constants.JwtConstants;
import com.arquivin.framework.utils.jwt.filter.CORSFilter;
import com.arquivin.framework.utils.jwt.filter.JWTAuthenticationFilter;
import com.arquivin.framework.utils.jwt.filter.JWTLoginFilter;

@Configuration
@EnableWebSecurity
@Order(value=100)
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityCfg extends WebSecurityConfigurerAdapter {
	
	@Autowired(required=false)
	private PBEPasswordEncoder passwordEnconder;
	
	private static String ROLE_PREFIX = "ROLE_";
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable().authorizeRequests()
		.antMatchers("/home").permitAll()
		.antMatchers(HttpMethod.POST, "/login").permitAll()
		.anyRequest().authenticated()
		.and()
		.addFilterBefore(new CORSFilter(), UsernamePasswordAuthenticationFilter.class)
		// filtra requisições de login
		.addFilterBefore(new JWTLoginFilter("/login", authenticationManager()),
                UsernamePasswordAuthenticationFilter.class)
		
		// filtra outras requisições para verificar a presença do JWT no header
		.addFilterBefore(new JWTAuthenticationFilter(),
                UsernamePasswordAuthenticationFilter.class);

	}

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.jdbcAuthentication().dataSource(dataSource())
		.rolePrefix(ROLE_PREFIX)
		.usersByUsernameQuery(JwtConstants.USER_BY_USERNAME_QUERY)
		.authoritiesByUsernameQuery(JwtConstants.AUTHORITIES_BY_USERNAME_QUERY)
		.passwordEncoder(passwordEnconder);
	}
	
	/**
	 * Necessario sobreescrever
	 * @return
	 */
	public DataSource dataSource() {
		return null;
	}
	
}
