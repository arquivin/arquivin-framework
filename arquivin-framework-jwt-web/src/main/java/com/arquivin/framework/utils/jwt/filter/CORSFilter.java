package com.arquivin.framework.utils.jwt.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.filter.OncePerRequestFilter;


/*
 * To enable CORS support in Spring MVC - Access-Control-Allow-Origin
 */
public class CORSFilter extends OncePerRequestFilter {

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
		response.setHeader("Access-Control-Allow-Origin", "*");
//		response.addHeader("Access-Control-Expose-Headers ", TokenAuthenticationService.HEADER_AUTHORIZATION);
//		response.addHeader("Access-Control-Expose-Headers ", TokenAuthenticationService.HEADER_EXPIRATION);
		response.addHeader("Access-Control-Expose-Headers ", "*");
		if (request.getHeader("Access-Control-Request-Method") != null && "OPTIONS".equals(request.getMethod())) {
			// CORS "pre-flight" request
			response.addHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
			response.addHeader("Access-Control-Allow-Headers", "Content-Type");
			response.addHeader("Access-Control-Max-Age", "1");
			response.addHeader("Access-Control-Allow-Headers", "Authorization");

			return;
		}

		filterChain.doFilter(request, response);

	}
}
