package com.arquivin.framework.utils.jwt.constants;

public class JwtConstants {

	private JwtConstants(){}
	
	public static final String USER_BY_USERNAME_QUERY = "SELECT p.login,p.passwordWeb,not(coalesce(inactive, false)) FROM person p WHERE p.login = ?";
	
	public static final String AUTHORITIES_BY_USERNAME_QUERY = "SELECT p.login,g.name FROM person p, accessgroup g WHERE p.fk_accessgroup = g.id_accessgroup AND p.login = ?";
}
