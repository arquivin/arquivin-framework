package com.arquivin.framework.utils.jwt.model;

import java.util.Collection;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

public class ArquivinUsernamePasswordAuthenticationToken extends UsernamePasswordAuthenticationToken{

	private String subDomain;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1856148867986769976L;

	public ArquivinUsernamePasswordAuthenticationToken(Object principal, Object credentials,	Collection<? extends GrantedAuthority> authorities, String subDomain) {
		super(principal, credentials, authorities);
		this.subDomain = subDomain;
	}
	
	public ArquivinUsernamePasswordAuthenticationToken(Object principal, Object credentials, String subDomain) {
		super(principal, credentials);
		this.subDomain = subDomain;
	}

	public String getSubDomain() {
		return subDomain;
	}

	public void setSubDomain(String subDomain) {
		this.subDomain = subDomain;
	}
	
}
