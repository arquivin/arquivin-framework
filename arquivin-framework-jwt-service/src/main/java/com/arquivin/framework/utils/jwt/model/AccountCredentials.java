package com.arquivin.framework.utils.jwt.model;

import java.io.Serializable;

public class AccountCredentials implements Serializable{

	private static final long serialVersionUID = -352882151726432149L;

	private String login;
	
	private String password;
	
	private Boolean rememberMe;
	
	private String subDomain;

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Boolean getRememberMe() {
		return rememberMe;
	}

	public void setRememberMe(Boolean rememberMe) {
		this.rememberMe = rememberMe;
	}

	public String getSubDomain() {
		return subDomain;
	}

	public void setSubDomain(String subDomain) {
		this.subDomain = subDomain;
	}
	
}
