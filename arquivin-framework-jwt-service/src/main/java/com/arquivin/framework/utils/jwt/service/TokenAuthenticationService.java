package com.arquivin.framework.utils.jwt.service;

import java.io.IOException;
import java.net.URI;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import com.arquivin.framework.utils.jwt.model.ArquivinUsernamePasswordAuthenticationToken;
import com.arquivin.framework.utils.jwt.model.Token;
import com.google.gson.Gson;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class TokenAuthenticationService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TokenAuthenticationService.class);

	public static final Long EXPIRATION_TIME = Long.parseLong(System.getProperty("arquivin.tokenExpiration", "999999999"));
	public static final String SECRET = System.getProperty("arquivin.secret", "123456");
	public static final String TOKEN_PREFIX = "Bearer";
	public static final String HEADER_EXPIRATION = "expiration";
	public static final String HEADER_AUTHORIZATION = "authorization";
	public static final String AUTHORITY = "authority";
	public static final String SUB_DOMAIN = "subDomain";
	public static final Long EXPIRATION_TIME_REMEMBER_ME = 31_556_952_000l;
	public static final Gson gson = new Gson();

	public static void addAuthentication(HttpServletResponse response, String username, Collection<? extends GrantedAuthority> roles, Boolean rememberMe, String subDomain) throws IOException {
		String responseTkn = createTokenJWT(username, roles, rememberMe, subDomain);
		
		response.getWriter().write(responseTkn);
		response.getWriter().flush();
		response.getWriter().close();
	}

	/**
	 * Cria o token JWT
	 * @param username
	 * @param roles
	 * @param rememberMe
	 * @param subDomain 
	 * @param subDomain 
	 * @return
	 */
	public static String createTokenJWT(String username, Collection<? extends GrantedAuthority> roles, Boolean rememberMe, String subDomain) {
		Long expiration = getExpiration(rememberMe);
		if (subDomain == null) {
			subDomain = "";
		}
		String jwt = Jwts.builder().setSubject(username)
				.claim(SUB_DOMAIN, subDomain)
				.claim(AUTHORITY, roles)
				.setExpiration(new Date(System.currentTimeMillis() + expiration))
				.signWith(SignatureAlgorithm.HS512, SECRET+"_"+subDomain).compact();

		return createResponseJsonToken(TOKEN_PREFIX + " " + jwt, expiration, rememberMe);
	}
	
	
	/**
	 * Recupera o tempo de expiracao.
	 * @param rememberMe
	 * @return
	 */
	private static Long getExpiration(Boolean rememberMe) {
		if(rememberMe != null && rememberMe) {
			return EXPIRATION_TIME_REMEMBER_ME;
		}
		return EXPIRATION_TIME;
	}

	public static String createResponseJsonToken(String token, Long expiration, Boolean rememberMe){
		Token tkn = new Token();
		tkn.setToken(token);
		tkn.setExpiration(expiration);
		tkn.setRememberMe(rememberMe);
		Gson gson = new Gson();
		return gson.toJson(tkn);
	}

	public static Authentication getAuthentication(HttpServletRequest request) {
		LOGGER.info("BEGIN");
		String token = request.getHeader(HEADER_AUTHORIZATION);
		LOGGER.info(String.format("token [%s]", token));
		if (token != null) {
			// faz parse do token
			String subDomain = "";
			String referer = request.getHeader("referer");
			LOGGER.info(String.format("referer [%s]", referer));
			if (referer != null && referer.contains(".")) {
//				subDomain = referer.split("\\.")[0].replaceAll("http://", "").replaceAll("https://", "").replaceAll("www.", "");
				subDomain = URI.create(referer).getHost().split("\\.")[0];
			}
			LOGGER.info(String.format("subDomain [%s]", subDomain));
			Jws<Claims> jwsClaims = Jwts.parser().setSigningKey(SECRET+"_"+subDomain).parseClaimsJws(token.replace(TOKEN_PREFIX, ""));
			String user = jwsClaims.getBody().getSubject();
			if (user != null) {
				return new ArquivinUsernamePasswordAuthenticationToken(user, null, getAuthorities(jwsClaims), subDomain);
			}
		}
		LOGGER.info("END");
		return null;
	}

	@SuppressWarnings("rawtypes")
	private static Collection<GrantedAuthority> getAuthorities(Jws<Claims> jwsClaims) {
		Collection<GrantedAuthority> authorities = null;
		List roles = (List) jwsClaims.getBody().get(AUTHORITY);
		if(roles != null && !roles.isEmpty()){
			authorities = new HashSet<>(roles.size());
			for(Object roleHashMap : roles) {
				Map authMap = (Map) roleHashMap;
				authorities.add(new SimpleGrantedAuthority((String)authMap.get(AUTHORITY)));
			}
		}else{
			authorities = new HashSet<>(0);
		}
		return authorities;
	}

	public static void main(String[] args) {
		String referer = "http://bco.espacobelvedere.com.br/";
		System.out.println(URI.create(referer).getHost().split("\\.")[0]);
		System.out.println(referer.split("\\.")[0].replaceAll("http://", "").replaceAll("https://", "").replaceAll("www.", ""));
	}
	
}
