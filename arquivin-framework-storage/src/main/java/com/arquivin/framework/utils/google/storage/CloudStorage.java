package com.arquivin.framework.utils.google.storage;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.IOUtils;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.InputStreamContent;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.storage.Storage;
import com.google.api.services.storage.StorageScopes;
import com.google.api.services.storage.model.Bucket;
import com.google.api.services.storage.model.StorageObject;
import com.google.auth.Credentials;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.HttpMethod;
import com.google.cloud.storage.Storage.SignUrlOption;
import com.google.cloud.storage.StorageOptions;

/**
 * Simple wrapper around the Google Cloud Storage API
 */
public class CloudStorage {

	private static Properties properties;

	private static Storage storage;

	private static final String PROJECT_ID_PROPERTY = "project.id";

	private static final String APPLICATION_NAME_PROPERTY = "application.name";

	private static final String ACCOUNT_ID_PROPERTY = "account.id";

	private static final String PRIVATE_KEY_PATH_PROPERTY = "private.key.path";

	private static final String PRIVATE_JSON_PATH_PROPERTY = "private.json.path";

	/**
	 * Uploads a file to a bucket. Filename and content type will be based on the
	 * original file.
	 * 
	 * @param bucketName
	 *            Bucket where file will be uploaded
	 * @param filePath
	 * @param inputStream
	 * @throws Exception
	 */
	@Deprecated
	public static void uploadFile(StorageBucketName bucketName, String newFileName, InputStream inputStream)
			throws Exception {
		uploadFile(bucketName.toString(), newFileName, inputStream);
	}
	
	/**
	 * Uploads a file to a bucket. Filename and content type will be based on the
	 * original file.
	 * 
	 * @param bucketName
	 *            Bucket where file will be uploaded
	 * @param filePath
	 * @param inputStream
	 * @throws Exception
	 */
	public static String uploadFile(String bucketName, String newFileName, InputStream inputStream)
			throws Exception {

		Storage storage = getStorage();

		StorageObject object = new StorageObject();

		object.setBucket(bucketName);

		try {

			String contentType = URLConnection.guessContentTypeFromStream(inputStream);

			InputStreamContent content = new InputStreamContent(contentType, inputStream);

			Storage.Objects.Insert insert = storage.objects().insert(bucketName.toString(), null, content);
			insert.setPredefinedAcl("publicRead");
			insert.setName(newFileName);

			insert.execute();
			
			return String.format("https://storage.googleapis.com/%s/%s", bucketName, newFileName);
		} finally {
			inputStream.close();
		}
	}

	/**
	 * Uploads a file to a bucket. Filename and content type will be based on the
	 * original file.
	 * 
	 * @param bucketName
	 *            Bucket where file will be uploaded
	 * @param filePath
	 * @param inputStream
	 * @throws Exception
	 */
	public static void uploadFilePrivate(StorageBucketName bucketName, String newFileName, InputStream inputStream)
			throws Exception {

		Storage storage = getStorage();

		StorageObject object = new StorageObject();

		object.setBucket(bucketName.toString());

		try {

			String contentType = URLConnection.guessContentTypeFromStream(inputStream);

			InputStreamContent content = new InputStreamContent(contentType, inputStream);

			Storage.Objects.Insert insert = storage.objects().insert(bucketName.toString(), null, content);
			insert.setName(newFileName);

			insert.execute();

		} finally {
			inputStream.close();
		}
	}

	/**
	 * Retorna um arquivo.
	 * 
	 * @param fileName
	 * @return
	 */
	public static byte[] getFileMetadata(StorageBucketName bucketName, String fileName) throws Exception {

		Storage storage = getStorage();
		Storage.Objects.Get get = storage.objects().get(bucketName.toString(), fileName);

		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		try {
			get.executeAndDownloadTo(stream);
			return stream.toByteArray();
		} finally {
			stream.close();
		}
	}


	/**
	 * Lists the objects in a bucket
	 * 
	 * @param bucketName
	 *            bucket name to list
	 * @return Array of object names
	 * @throws Exception
	 */

	public static List<String> listBucket(String bucketName) throws Exception {

		Storage storage = getStorage();

		List<String> list = new ArrayList<String>();

		List<StorageObject> objects = storage.objects().list(bucketName).execute().getItems();
		if (objects != null) {
			for (StorageObject o : objects) {
				list.add(o.getName());
			}
		}
		return list;

	}

	/**
	 * List the buckets with the project (Project is configured in properties)
	 * 
	 * @return
	 * @throws Exception
	 */

	public static List<String> listBuckets() throws Exception {

		Storage storage = getStorage();

		List<String> list = new ArrayList<String>();

		List<Bucket> buckets = storage.buckets().list(getProperties().getProperty(PROJECT_ID_PROPERTY)).execute()
				.getItems();

		if (buckets != null) {
			for (Bucket b : buckets) {
				list.add(b.getName());
			}
		}
		return list;
	}

	/**
	 * Recupera o arquivo de properties de configuracoes
	 * 
	 * @return
	 * @throws Exception
	 */
	private static Properties getProperties() throws Exception {

		if (properties == null) {
			properties = new Properties();
			InputStream stream = CloudStorage.class.getClassLoader()
					.getResourceAsStream("google/cloudstorage.properties");

			try {
				properties.load(stream);
			} catch (IOException e) {
				throw new RuntimeException("cloudstorage.properties must be present in classpath", e);
			} finally {
				stream.close();
			}

		}
		return properties;
	}

	/**
	 * Retorna um storage logado.
	 * 
	 * @return
	 * @throws Exception
	 */
	private static Storage getStorage() throws Exception {

		final File tempFile = File.createTempFile("stream2file", ".tmp");
		if (storage == null) {

			HttpTransport httpTransport = new NetHttpTransport();

			JsonFactory jsonFactory = new JacksonFactory();
			List<String> scopes = new ArrayList<String>();
			scopes.add(StorageScopes.DEVSTORAGE_FULL_CONTROL);
			String pathFileKey = getProperties().getProperty(PRIVATE_KEY_PATH_PROPERTY);
			InputStream url = CloudStorage.class.getClassLoader().getResourceAsStream(pathFileKey);

			try (FileOutputStream out = new FileOutputStream(tempFile)) {
				IOUtils.copy(url, out);
			}

			Credential credential = new GoogleCredential.Builder().setTransport(httpTransport)
					.setJsonFactory(jsonFactory).setServiceAccountId(getProperties().getProperty(ACCOUNT_ID_PROPERTY))
					.setServiceAccountPrivateKeyFromP12File(tempFile).setServiceAccountScopes(scopes).build();

			storage = new Storage.Builder(httpTransport, jsonFactory, credential)
					.setApplicationName(getProperties().getProperty(APPLICATION_NAME_PROPERTY)).build();
		}
		tempFile.deleteOnExit();
		tempFile.delete();

		return storage;

	}
	
	/**
	 * Cria a url para baixar do arquivo
	 * @param bucket
	 * @param storageFileName
	 * @return
	 * @throws Exception 
	 */
	public static String createGetUrl(String bucket, String storageFileName, Integer expirationTimeInMinutes) throws Exception {
		return signUrl(HttpMethod.GET, bucket, storageFileName, null, expirationTimeInMinutes);
	}
	
	/**
	 * Cria a url para uploaddo arquivo
	 * @param bucket
	 * @param storageFileName
	 * @return
	 * @throws Exception 
	 */
	public static String createPutUrl(String bucket, String storageFileName, String contentType, Integer expirationTimeInMinutes) throws Exception {
		return signUrl(HttpMethod.PUT, bucket, storageFileName, contentType, expirationTimeInMinutes);
	}

	/**
	 * Gera uma url assinada
	 * @param method
	 * @param bucket
	 * @param storageFileName
	 * @param expirationTimeInMinutes
	 * @throws Exception 
	 */
	private static String signUrl(HttpMethod method, String bucket, String storageFileName, String contentType, Integer expirationTimeInMinutes) throws Exception {
		String pathFileKey = getProperties().getProperty(PRIVATE_JSON_PATH_PROPERTY);
		InputStream is = CloudStorage.class.getClassLoader().getResourceAsStream(pathFileKey);
		Credentials credentials = GoogleCredentials.fromStream(is);
		com.google.cloud.storage.Storage storage = StorageOptions.newBuilder().setCredentials(credentials).build().getService();
		if(HttpMethod.GET.equals(method)) {
			URL signedUrl = storage.signUrl(BlobInfo.newBuilder(bucket, storageFileName).build(), expirationTimeInMinutes,  TimeUnit.SECONDS);
			return signedUrl.toString();
		}
		SignUrlOption optionsPost = com.google.cloud.storage.Storage.SignUrlOption.httpMethod(method);
		SignUrlOption optionsContentType = com.google.cloud.storage.Storage.SignUrlOption.withContentType();
		BlobId blobId = BlobId.of(bucket, storageFileName);
        BlobInfo blobInfo = BlobInfo.newBuilder(blobId).setContentType(contentType).build();
		URL signedUrl = storage.signUrl(blobInfo, expirationTimeInMinutes,  TimeUnit.SECONDS, optionsPost, optionsContentType);
		return signedUrl.toString();
	}

}