package com.arquivin.framework.utils.google.storage;


/**
 * Armazena os Buckets da Google Storage
 * @author Caio
 *
 */
public enum StorageBucketName {
    DEMO("Demo"),
    ARQUIVIN("arquivin");

    private String value;

    StorageBucketName(String str) {
        value = str;
    }

    public String toString() {
        return value;
    }
    
}
