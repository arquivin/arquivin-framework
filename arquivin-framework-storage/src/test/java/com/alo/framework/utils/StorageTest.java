
package com.alo.framework.utils;
/*
 * Copyright 2015 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.net.URLEncoder;

// [START all]

import org.junit.Test;

import com.arquivin.framework.utils.google.storage.CloudStorage;

public class StorageTest {
  
  @Test
  public void testGetFile() throws Exception {
//	 byte[] b = CloudStorage.getFile(StorageBucketName.SGA_INFO, "layout_ingresso_OLD.png");
//	 System.out.println(b.length);
  }
  
  @Test
  public void signUrl() throws Exception {
	  try {
		  String url = CloudStorage.createGetUrl("arquivin", "chart.jpeg", 10);
		  url = url+"&response-content-disposition="+URLEncoder.encode("attachment;filename=batata.jpeg, \"UTF-8\"");
		  System.out.println("Get : "+url);
		  url = CloudStorage.createPutUrl("arquivin", "chart.jpeg","image/jpeg", 10);
		  System.out.println("Put : "+url);
	  }catch (Exception e) {
		e.printStackTrace();
	}
  }

}
